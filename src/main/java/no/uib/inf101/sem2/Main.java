package no.uib.inf101.sem2;
import no.uib.inf101.sem2.BattleShip.controller.BattleShipController;
import no.uib.inf101.sem2.BattleShip.controller.MouseController;
import no.uib.inf101.sem2.BattleShip.model.BattleShipModel;
import no.uib.inf101.sem2.BattleShip.model.Board;
import no.uib.inf101.sem2.BattleShip.model.Ships.NextShipFactory;
import no.uib.inf101.sem2.BattleShip.model.Ships.ShipFactory;
import no.uib.inf101.sem2.BattleShip.view.BattleShipView;

import javax.swing.JFrame;

public class Main{
  public static void main(String[] args) {
    Board setupboard1 = new Board(15, 15);
    Board setupboard2 = new Board(15, 15);
    Board attackboard1 = new Board(15, 15);
    Board attackboard2 = new Board(15, 15);
    ShipFactory ships1 = new NextShipFactory();  
    //ShipFactory ships2 = new NextShipFactory();
    BattleShipModel model = new BattleShipModel(setupboard1, setupboard2, attackboard1, attackboard2, ships1);
    BattleShipView view = new BattleShipView(model);  
    BattleShipController controller = new BattleShipController(model, view); 
    MouseController mouseController = new MouseController(model, view);
  

     

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101 BATTLESHIP");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);

  
  }
}

  