package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;

public class Grid<E> implements IGrid<E> {
    

private int rows;
private int cols;
protected ArrayList<ArrayList<E>> grid;

//kontroktør 1
public Grid(int rows, int cols){
    this(rows, cols, null);     
}
//konstruktør 2
public Grid(int rows, int cols, E defaultvalue){

    this.rows = rows;
    this.cols = cols;
    this.grid = new ArrayList<>();

    for (int r =0; r < rows; r++){
        ArrayList<E> singleRow = new ArrayList<>();
    
        for (int c = 0; c < cols; c++){
         singleRow.add(defaultvalue);
          
        }
    this.grid.add(singleRow);
    }
}

@Override
public int rows() {
    return this.grid.size();
}

@Override
public int cols() {
    return this.grid.get(0).size();}

@Override
public Iterator<GridCell<E>> iterator() {
    ArrayList<GridCell<E>> grid = new ArrayList<GridCell<E>>();
    
    for (int i =0; i < rows; i++){
    for (int j=0; j<cols; j++){
        CellPosition pos = new CellPosition(i, j);
        E value = get(pos);
        grid.add(new GridCell<E>(pos, value));
  }
}
return grid.iterator();

}

@Override
public void set(CellPosition pos, E value) {
    ArrayList<E> row = this.grid.get(pos.row());
    row.set(pos.col(), value);
}


@Override
public boolean positionIsOnGrid(CellPosition pos) {
    if (pos.row() < 0) return false;
    if (pos.col() < 0) return false;
    if (pos.row() >= this.rows()) return false;
    if (pos.col() >= this.cols()) return false;
    return true;

}

   
@Override
public E get(CellPosition pos) {
    ArrayList<E> row = this.grid.get(pos.row());
    return row.get(pos.col());
}


}