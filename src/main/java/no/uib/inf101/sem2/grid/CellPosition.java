package no.uib.inf101.sem2.grid;

  /**
 * Cellposition holds two integers for a specific position on the grid
 * @param row on grid
 * @param col on grid
 * @returns two integers, a position on grid
 */
public record CellPosition(int row, int col){} 

