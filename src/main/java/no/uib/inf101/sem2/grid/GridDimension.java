package no.uib.inf101.sem2.grid;

public interface GridDimension {
    /** Number of rows in the grid 
    * @returns integer
    */
    int rows();

    /** Number of columns in the grid 
     * @returns integer
     */
    int cols();
}
    

