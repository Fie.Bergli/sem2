package no.uib.inf101.sem2.BattleShip.model;

public enum GameState {
    WELCOME, SET_UP, ATTACK, YOU_WON, WAITING
}
