package no.uib.inf101.sem2.BattleShip.view;
import no.uib.inf101.sem2.BattleShip.model.GameState;
import no.uib.inf101.sem2.BattleShip.model.PlayerState;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public interface ViewableBattleshipModel {
     /** getDimension() gets the number rows and number colums in the board 
     * @return a board with x rows and columns 
    */
    GridDimension getDimension();  
    
    /** getTilesOnBoard iterates over all tiles on the set up board, 
     * @returns a board with x rows and columns  
     */
    Iterable<GridCell<Character>> getTilesOnSetUpBoard();

    /** Iterates all tiles in the Ship
     * @returns a new Ship
     */
    Iterable<GridCell<Character>> getShipsOnBoard();

    /**
     * gets the state of the game
     * @return gamestate 
     */
    GameState getGameState();

    /**
     * gets the player state, which player is playing the game
     * @returns player state 
     */
    PlayerState getPlayerState();

    /** getTilesOnBoard iterates over all tiles on the attack board, 
     * @returns a board with x rows and columns  
     */
    Iterable<GridCell<Character>> getTilesOnAttackBoard();

    /**
     * Takes a shot at the given cell position on the inactive SET_UP board. 
     * Updates the active ATTACK board with 'h' if the shot hits a ship or with 'm' if it misses. 
     * Prints a message to the console indicating whether the shot hit or missed.
     * @param pos the cell position to shoot at
     */
    boolean shotsFired(CellPosition pos);

    
}
