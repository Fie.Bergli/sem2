package no.uib.inf101.sem2.BattleShip.model.Ships;



public class NextShipFactory implements ShipFactory{

    private final String ALLSHIPS = "12345";
    private char symbol;
    private int index = 0;
   
    @Override
    public Ships getNextShipsPlayer() {
        if (index >= ALLSHIPS.length()) {
            index = 0;
        }
        symbol = ALLSHIPS.charAt(index);
        index += 1;
        return Ships.newShips(symbol);
    }

    
    
    @Override
        public void reset(){
            index = 0;
        }

    @Override
    public int getIndex() {
        return this.index;
}
}












