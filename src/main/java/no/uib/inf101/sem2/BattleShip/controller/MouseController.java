package no.uib.inf101.sem2.BattleShip.controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import javax.swing.Timer;
import no.uib.inf101.sem2.BattleShip.model.GameState;
import no.uib.inf101.sem2.BattleShip.view.BattleShipView;
import no.uib.inf101.sem2.BattleShip.view.CellPositionToPixelConverter;
import no.uib.inf101.sem2.grid.CellPosition;


public class MouseController implements MouseListener{

        private BattleShipView view;
        private ContrallableBattleShipModel battleShipModel;
    
        public MouseController(ContrallableBattleShipModel battleShipModel, BattleShipView view){
            this.battleShipModel = battleShipModel;
            this.view = view;
            view.addMouseListener(this);
        }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (battleShipModel.getGameState() == GameState.ATTACK){
            Point2D point2 = e.getPoint();
        CellPositionToPixelConverter converter = this.view.getCellPositionToPixelConverter();
        CellPosition position = converter.getCellPositionOfPoint(point2);
        if (battleShipModel.shotsFired(position)){
            this.battleShipModel.setCellPosition(position);
            this.battleShipModel.allShipsAreDown();
        
        view.repaint();

        if (battleShipModel.getGameState()==GameState.ATTACK){
            battleShipModel.setGameState(GameState.WAITING);
            Timer timer = new Timer(1000, new ActionListener() {


                @Override
                public void actionPerformed(ActionEvent e) {
                    battleShipModel.changePlayerInAttackMode();
                    view.repaint();
                    battleShipModel.setGameState(GameState.ATTACK);
                }
            });
            timer.setInitialDelay(1000);
            timer.setRepeats(false);
            timer.start();
        }
        }

        }
        
    }
  
    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}



}   