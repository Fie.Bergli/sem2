package no.uib.inf101.sem2.BattleShip.model;

public enum PlayerState {
    PLAYER_1, PLAYER_2
}
