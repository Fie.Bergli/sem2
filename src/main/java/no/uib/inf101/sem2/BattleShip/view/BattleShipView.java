package no.uib.inf101.sem2.BattleShip.view;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;
import java.awt.Font;
import no.uib.inf101.sem2.BattleShip.model.PlayerState;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;


public class BattleShipView extends JPanel{

  private ViewableBattleshipModel model;
  private ColorTheme color; 
  private static final double OUTERMARGIN = 40;
  private static final double INNERMARGIN = 2;
    
    
    
  public BattleShipView(ViewableBattleshipModel model){
    this.model = model;
    this.setPreferredSize(new Dimension(500, 600));
    this.setFocusable(true);
    this.color = new DefaultColorTheme(model);
    this.setBackground(color.getBackgroundColor());
    }
  
  

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawCurrentState(g2);
    }

    /**
     * Paints the game board and current state of the game.
     * @param g the Graphics object to paint on
     */
  private void drawCurrentState(Graphics2D g){
    switch (model.getGameState()) {
            
      case WELCOME:
        drawWelcomeState(g);
        break;
      case SET_UP:
        drawSetUp(g, model.getPlayerState());
        break;
      case ATTACK, WAITING:
        drawAttackState(g, model.getPlayerState());
        break;
      case YOU_WON:
        drawYouWonState(g, model.getPlayerState());
        break;
      }
    }

  private void drawWelcomeState( Graphics2D canvas){
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    canvas.setColor(Color.BLUE);
    canvas.setFont(new Font("Serif", Font.BOLD, 40));
    Inf101Graphics.drawCenteredString(canvas, "BATTLESHIP", OUTERMARGIN, OUTERMARGIN, width, height);
    canvas.setFont(new Font("Serif", Font.BOLD, 20));
    Inf101Graphics.drawCenteredString(canvas, "Press '1' to place ships", OUTERMARGIN, OUTERMARGIN, width, height + 80);    
    }  
    
  private void drawSetUp(Graphics2D canvas, PlayerState player){
      double x = OUTERMARGIN;
      double y = OUTERMARGIN;
      double width = this.getWidth() - 2 * OUTERMARGIN;
      double height = this.getHeight() - 2 * OUTERMARGIN;
      this.setBackground(color.getBackgroundColor());
      Color cellColor = color.getFrameColor();
      Rectangle2D rectangle = new Rectangle2D.Double(x, y, width, height);
      canvas.setColor(cellColor);
      canvas.fill(rectangle);
      CellPositionToPixelConverter cellPosition = new CellPositionToPixelConverter(rectangle, model.getDimension(), INNERMARGIN);
      drawCells(canvas, model.getTilesOnSetUpBoard(), cellPosition, color);
      drawCells(canvas, model.getShipsOnBoard(), cellPosition, color);
      canvas.setColor(Color.WHITE);
      canvas.setFont(new Font("Serif", Font.BOLD, 20));
    if (player == PlayerState.PLAYER_1){
      canvas.setFont(new Font("Serif", Font.BOLD, 20));
      Inf101Graphics.drawCenteredString(canvas, "PLAYER 1 Set-Up phase", OUTERMARGIN, OUTERMARGIN, width, -20); 
    }
    if (player == PlayerState.PLAYER_2){
      Inf101Graphics.drawCenteredString(canvas, "PLAYER 2 Set-Up phase", OUTERMARGIN, OUTERMARGIN, width, -20); 
    }
    
  }

  private void drawAttackState(Graphics2D canvas, PlayerState player){
      double x = OUTERMARGIN;
      double y = OUTERMARGIN;
      this.setBackground(color.getBackgroundColor());
      double width = this.getWidth() - 2 * OUTERMARGIN;
      double height = this.getHeight() - 2 * OUTERMARGIN;
      Color cellColor = color.getFrameColor();
      Rectangle2D rectangle = new Rectangle2D.Double(x, y, width, height);
      canvas.setColor(cellColor);
      canvas.fill(rectangle);
      CellPositionToPixelConverter cellPosition = new CellPositionToPixelConverter(rectangle, model.getDimension(), INNERMARGIN);
      drawCells(canvas, model.getTilesOnAttackBoard(), cellPosition, color);
      canvas.setFont(new Font("Serif", Font.BOLD, 20));
      canvas.setColor(Color.WHITE);

    if (player == PlayerState.PLAYER_1){
      Inf101Graphics.drawCenteredString(canvas, "PLAYER 1, attack your enemy`s ships", OUTERMARGIN, OUTERMARGIN, width, -20);
      }
    if (player == PlayerState.PLAYER_2){
      Inf101Graphics.drawCenteredString(canvas, "PLAYER 2, attack your enemy`s ships", OUTERMARGIN, OUTERMARGIN, width, -20);
      }
  }

  private void drawYouWonState(Graphics2D canvas,PlayerState player){ 
    drawAttackState(canvas, player);
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    Color cellColor = color.gameStateColor();
    Rectangle2D rectangle = new Rectangle2D.Double(0, 0, getWidth(), getHeight());
    canvas.setColor(cellColor);
    canvas.fill(rectangle);
    canvas.setFont(new Font("Serif", Font.BOLD, 20));
    canvas.setColor(Color.WHITE);
    if (player == PlayerState.PLAYER_1){
    Inf101Graphics.drawCenteredString(canvas, "Congrats PLAYER 1, you won Battleship!", OUTERMARGIN, OUTERMARGIN, width, height);}
    if(player == PlayerState.PLAYER_2){
      Inf101Graphics.drawCenteredString(canvas, "Congrats PLAYER 2, you won Battleship!", OUTERMARGIN, OUTERMARGIN, width, height);}
  }
   
  

  
    /** Draws the collection of cells on the board, uses a loop to get
     * right cell position and color
     * @param canvas used to draw cells
     * @param grid holds the collection of cells
     * @param cellPosition a rectangle "cell" with a specific placement on grid
     * @param colors holds cell color 
    */
  public void drawCells(Graphics2D canvas, Iterable<GridCell<Character>> grid,CellPositionToPixelConverter cellPosition, ColorTheme colors){
    for (GridCell<Character> singleCell : grid){
      Rectangle2D rectangle = cellPosition.getBoundsForCell(singleCell.pos());
          
        Color color = colors.getCellColor(singleCell.value());
  
        canvas.setColor(color);
        canvas.fill(rectangle);
        }
      }
  
  /**
   * Method is written by Torstein Strømme in given code clickableGrid.
   * Gets an object which converts between CellPosition in a grid and 
   * their pixel positions on the screen.
   */
  public CellPositionToPixelConverter getCellPositionToPixelConverter() {
    Rectangle2D bounds = new Rectangle2D.Double(
        OUTERMARGIN,
        OUTERMARGIN,
        this.getWidth() - 2 * OUTERMARGIN,
        this.getHeight() - 2 * OUTERMARGIN);
    GridDimension gridSize = this.model.getDimension();
    return new CellPositionToPixelConverter(bounds, gridSize, INNERMARGIN);
  }
}

    

