package no.uib.inf101.sem2.BattleShip.controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.Timer;
import no.uib.inf101.sem2.BattleShip.view.BattleShipView;



public class BattleShipController  implements  java.awt.event.KeyListener{

    private ContrallableBattleShipModel battleShipModel;
    private BattleShipView view;
 

   public BattleShipController(ContrallableBattleShipModel battleShipModel, BattleShipView view){
    view.setFocusable(true);
    this.battleShipModel = battleShipModel;
    this.view = view;
    //this.timer = new Timer(battleShipModel.getSeconds(), this::clockTick);
    view.addKeyListener(this);
    
   }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            battleShipModel.moveShip(0,-1);
        }
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            battleShipModel.moveShip(0,1);
        }
        else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            battleShipModel.moveShip(1, 0);
        }
        else if (e.getKeyCode() == KeyEvent.VK_UP) {
            battleShipModel.moveShip(-1, 0);
        }
        else if (e.getKeyCode() == KeyEvent.VK_R){
            battleShipModel.rotateShip();
        }
       
        else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            battleShipModel.gluedShip();

            if (battleShipModel.shipsPlaced() == 5){
                Timer timer = new Timer(2000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        battleShipModel.changePlayerInSetUp();
                        view.repaint();
                    }
                });
                timer.setInitialDelay(2000);
                timer.setRepeats(false);
                timer.start();
            }
            if (battleShipModel.shipsPlaced() == 10){
                Timer timer = new Timer(2000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        battleShipModel.changeGameState();
                        view.repaint();
                    }
                });
                timer.setInitialDelay(2000);
                timer.setRepeats(false);
                timer.start();
            }
            
        }
        else if (e.getKeyCode() == KeyEvent.VK_1){
            battleShipModel.changeGameState();
            }        
        view.repaint();
    }
   


    @Override
    public void keyReleased(KeyEvent e) {}
}