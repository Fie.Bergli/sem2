package no.uib.inf101.sem2.BattleShip.view;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;

public class CellPositionToPixelConverter {
    private Rectangle2D box; 
    private GridDimension gd;
    private double margin;
    private double widthCell;
    private double heightCell;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){
  this.box = box;
  this.gd = gd;
  this.margin = margin;
  this.widthCell = (box.getWidth() - margin * gd.cols() - margin) / gd.cols();
  this.heightCell = (box.getHeight() - margin * gd.rows() - margin) / gd.rows();

  }        

  /**
   * Takes in a cellposition and calculates the pixel-coordinates 
   * to this cell. 
   * @param cp row and column number
   * @return a rectangle object with pixel coordinates 
   */
  public Rectangle2D getBoundsForCell(CellPosition cp){
  double cellWidth = (box.getWidth() - ((gd.cols()+1) * margin))/gd.cols(); 
  double cellHeight = (box.getHeight() - ((gd.rows()+1)*margin))/gd.rows();
          
  double cellX = box.getX()+ (margin * (cp.col()+1)) + (cellWidth*cp.col());
  double cellY = box.getY()+ (margin* (cp.row()+1)) + (cellHeight*cp.row());
             
  return (new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight)) ;
  }

  /**
   * Method is written by TorsteinStrømme in given code clickablegrid.
   * Takes in a point object that holds a position on the screen, 
   * and converts it to a cellposition on the battleship grid. 
   * Uses the same math as in getBoundsForCell. 
   * @param point two doubles, represents the point on screen
   * @return the corresponding CellPosition 
   */
  public CellPosition getCellPositionOfPoint(Point2D point) {
    // Same math as getBoundsForCell, but isolate the col/row on one side
    // and replace cellX with point.getX() (cellY with point.getY())
    double col = (point.getX() - box.getX() - margin) / (widthCell + margin);
    double row = (point.getY() - box.getY() - margin) / (heightCell + margin);

    // When row or col is out of bounds
    if (row < 0 || row >= gd.rows() || col < 0 || col >= gd.cols()){
      System.out.println("row or col is out of bounds");
      return null;
    } 
    
    // Verify that the point is indeed inside the bounds of the cell, and not on
    // the margin border
    CellPosition pos = new CellPosition((int) row, (int) col);
    if (getBoundsForCell(pos).contains(point)) {
      return pos;
    } else {
      System.out.println("the point is not inside the bounds of the cell");
      return null;
    }
  }    
}
