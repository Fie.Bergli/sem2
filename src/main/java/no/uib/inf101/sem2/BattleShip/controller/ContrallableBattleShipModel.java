package no.uib.inf101.sem2.BattleShip.controller;
import no.uib.inf101.sem2.BattleShip.model.GameState;
import no.uib.inf101.sem2.BattleShip.model.PlayerState;
import no.uib.inf101.sem2.grid.CellPosition;



public interface ContrallableBattleShipModel {
     /** 
     * Moves the current ship to a legal position on the board. A legal position means a cell which is within the boardframe,
     * and holds no other value
     * @param deltaRow number of rows added
     * @param deltaCol number of cols added
     * @returns true if the ship is moved, otherwise false
    */
    boolean moveShip(int deltaRow, int deltaCol);

    /** 
     * Rotates the Ship to the left 
     * @returns true when the ship sucsessfully rotated to a legal position on the board
     */
    boolean rotateShip();


    /**
     * Holds the state of the game. The different states are defined in the enum GameState.java
     * @return gamestate value 
     */
    GameState getGameState();

    /**
     * sets the current gamestate to the given gamestate object
     * @param game wished gamestate 
     */
    void setGameState( GameState game);

    /**
     * Gets the player state of game, which player is currently playing
     * @return playerstate value
     */
    PlayerState getPlayerState();

    /*
     * sets the current player state with given PlayerState object
     */
    void setPlayerState(PlayerState player);

    /**
     * sets the cellposition variabel with given cellposition value
     * @param pos wished cellposition
     */
    void setCellPosition(CellPosition pos);

    
    /**
     * Switches the active player in the setup phase of the game and performs relevant actions.
     * If the current player is PLAYER_1, the method switches to PLAYER_2 and sets the active board to the one
     * belonging to PLAYER_2. If the current player is PLAYER_2, the method switches to PLAYER_1 and sets the correct 
     * board to the one belonging to PLAYER_1. The method also freezes the game board if all ships are
     * placed on the board, and unfreezes otherwise. Finally, it resets the active shipfactory for player two. 
     * @return the PlayerState of the player who has become active after the switch (PLAYER_1 or PLAYER_2)
     */
    PlayerState changePlayerInSetUp();
    


    /**
    * Changes the game state to the next state.
    * If the current state is WELCOME, it changes to SET_UP.
    * If the current state is SET_UP, it changes to ATTACK.
    * @return the new game state after the change
    */
    GameState changeGameState();

   
    /**
     * Glues the ship in a specific position, if it is a legal position
     * A ship cannot be placed outside of the grid, or on top of another ship. 
     * The method also checks if all ships are placed on the board, and freezes the board if they are. 
     * Otherwise, it gets the next ship.
     */
    void gluedShip();

    /**
     * Takes a shot at the given cell position on the inactive SET_UP board. 
     * Updates the active ATTACK board with 'h' if the shot hits a ship or with 'm' if it misses. 
     * Prints a message to the console indicating whether the shot hit or missed.
     * @param pos the cell position to shoot at
     */
    boolean shotsFired(CellPosition pos);
    
    /**
     * Checks if all ships on the board have been sunk. If all the ships have been hit, it
     * sets the game state to "YOU_WON".
     * @return true if all ships are down and the game state has been set to "YOU_WON", false otherwise.
     */
    void allShipsAreDown();
    


    /**
     * Changes the player state from SEATTACK mode. 
     * Also changes the active boards from SET_UP to ATTACK board for the current player.
     * @return the new player state after the change
     */
    PlayerState changePlayerInAttackMode();

    int shipsPlaced();
   

    
    





    



    

}
