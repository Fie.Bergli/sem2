package no.uib.inf101.sem2.BattleShip.view;

import java.awt.Color;


import no.uib.inf101.sem2.BattleShip.model.PlayerState;

public class DefaultColorTheme implements ColorTheme{

    private ViewableBattleshipModel battleShipModel;
   

    public DefaultColorTheme(ViewableBattleshipModel model){
        this.battleShipModel = model;
    }

    @Override
    public Color getCellColor(Character shipName) {
        Color color = switch (shipName) {
            case '1' -> Color.MAGENTA;
            case '2' -> Color.ORANGE;
            case '3' -> Color.BLUE;
            case '4' -> Color.YELLOW;
            case '5' -> new Color(138,43,226);
            case '-' -> new Color(158, 217, 248);
            case 'm' -> Color.RED;
            case 'h' -> Color.GREEN;
            
    
            default -> throw new IllegalArgumentException(
            "No available color for '" + shipName + "'");  
           };
           return color;
        }
    

    @Override
    public Color getFrameColor() { return Color.WHITE;}

    @Override
    public Color getBackgroundColor() { 
        Color color = Color.BLUE;
        if ( battleShipModel.getPlayerState() == PlayerState.PLAYER_1){
            color = new Color(0,191,255);}
        if ( battleShipModel.getPlayerState() == PlayerState.PLAYER_2){
            color = new Color (65,105,225);}
        return color;
    }
            

    @Override
    public Color gameStateColor() {  return new Color(0, 0, 0, 128); }
}
    

