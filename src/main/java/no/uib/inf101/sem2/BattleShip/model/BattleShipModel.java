package no.uib.inf101.sem2.BattleShip.model;
import no.uib.inf101.sem2.BattleShip.controller.ContrallableBattleShipModel;
import no.uib.inf101.sem2.BattleShip.model.Ships.ShipFactory;
import no.uib.inf101.sem2.BattleShip.model.Ships.Ships;
import no.uib.inf101.sem2.BattleShip.view.ViewableBattleshipModel;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;


public class BattleShipModel implements ViewableBattleshipModel, ContrallableBattleShipModel{

    private Board attackBoard1;
    private Board attackBoard2;
    private Board setUpBoard1;
    private Board setUpBoard2;
    private Board activeSetUpBoard;
    private Board inactiveSetUpBoard;
    private Board activeAttackBoard;
    private Board inactiveAttackBoard;
    private boolean frozenBoard = false;
   
    private ShipFactory activeFactory;
    private Ships myShip;
    private GameState gameState;
    private PlayerState currentPlayer;
    private CellPosition currentCellPosition = null;
    private int shipsPlaced = 0;
    
    

    public BattleShipModel(Board setUpBoard1, Board setUpBoard2, Board attackBoard1, Board attackBoard2, ShipFactory ships) { 
        this.setUpBoard1 = setUpBoard1; 
        this.setUpBoard2 = setUpBoard2; 
        this.activeSetUpBoard = setUpBoard1;

        this.activeFactory = ships; 
        this.currentPlayer = PlayerState.PLAYER_1;

        this.attackBoard1 = attackBoard1;
        this.attackBoard2 = attackBoard2;
        this.activeAttackBoard = attackBoard1;

        this.myShip = this.activeFactory.getNextShipsPlayer().shiftedToStartPosition(activeSetUpBoard);       
        this.gameState = GameState.WELCOME;
    }
        
    
    @Override
    public Iterable<GridCell<Character>> getTilesOnSetUpBoard() {return activeSetUpBoard;}

    @Override
    public Iterable<GridCell<Character>> getShipsOnBoard() {return myShip;}

    @Override
    public Iterable<GridCell<Character>> getTilesOnAttackBoard() {return activeAttackBoard;}

    @Override
    public GridDimension getDimension() {return activeSetUpBoard;}

    @Override
    public GameState getGameState() {return gameState;}

    @Override
    public void setGameState(GameState game) {this.gameState = game;}

    @Override
    public PlayerState getPlayerState() {return currentPlayer; }

   @Override
    public void setPlayerState(PlayerState player) { this.currentPlayer = player; }
 
    @Override
    public void setCellPosition(CellPosition pos) {this.currentCellPosition = pos;}
    

    @Override
    public boolean moveShip(int deltaRow, int deltaCol){
        if (countNumberOfCellsWithShips()){
            Ships shipTest = myShip.shiftedBy(deltaRow, deltaCol);
            if (isLegalMove(shipTest)){
                myShip = shipTest;
                return true;
                } 
            }
        freezeBoard();
        return false;
    }

    private boolean cantPlace(Ships ship){
        for (GridCell<Character> cellInShip : ship){
            if (getPlayerState() == PlayerState.PLAYER_1 && setUpBoard1.get(cellInShip.pos()) != '-'){
                return false;}
            if (getPlayerState() == PlayerState.PLAYER_2 && setUpBoard2.get(cellInShip.pos()) != '-'){
                return false;}

            }
        return true;
    }

    private boolean isLegalMove(Ships gridShip){
        for (GridCell<Character> cellInShip : gridShip){
            if (!(activeSetUpBoard.positionIsOnGrid(cellInShip.pos()))){
                    return false;}
                }
        return true;
    }


    @Override
    public boolean rotateShip() {
        Ships shipTest = myShip.rotateShip();
        if (isLegalMove(shipTest)){
            myShip = shipTest;
            return true;
        }
        return false;
    }

    @Override
    public void gluedShip(){
        if (!cantPlace(myShip)){
            return;
        }
        for (GridCell<Character> cellPos : myShip){
            activeSetUpBoard.set(cellPos.pos(), cellPos.value());
        } 
        if (countNumberOfCellsWithShips()){
            getNewShip();
        } 
        else freezeBoard();
        shipsPlaced ++;
    }

    private void freezeBoard(){frozenBoard = true;}

    private void unfreezeBoard(){frozenBoard = false;}

    private void getNewShip() {
        Ships newShip = activeFactory.getNextShipsPlayer().shiftedToStartPosition(activeSetUpBoard);
        myShip = newShip;
    }
       
    
    private boolean countNumberOfCellsWithShips() {
        int count = 0;
        for (GridCell<Character> cell : activeSetUpBoard) {
            if (cell.value() != '-'){
                count++;
            }
            if (count >= 26){
                return false;
            }
        }
        return true; 
    }

    public void setActiveSetUpBoard(Board setUpBoard){this.activeSetUpBoard = setUpBoard;}

    private void setInactiveSetUpBoard(Board setupBoard){this.inactiveAttackBoard = setupBoard;}

    private void setActiveAttckBoard(Board attackboard){ this.activeAttackBoard = attackboard;}

    private void setInactiveAttckBoard(Board attackBoard){ this.inactiveAttackBoard = attackBoard;}


    @Override
    public GameState changeGameState(){
        switch (gameState) {
            case WELCOME:
                setPlayerState(PlayerState.PLAYER_1);
                setGameState(GameState.SET_UP);
                break;
            case SET_UP:
                setPlayerState(PlayerState.PLAYER_1);
                setGameState(GameState.ATTACK);
                break;
            default:
                break;
            }
        return gameState;
    }


    @Override
    public PlayerState changePlayerInSetUp() {
        if (getGameState() == GameState.SET_UP){
        switch (currentPlayer) {
            case PLAYER_1:
                // switch to player 2
                if (countNumberOfCellsWithShips() == false){
                    freezeBoard();
                } else unfreezeBoard(); 

                setActiveSetUpBoard(setUpBoard2); 
                setInactiveSetUpBoard(setUpBoard1);
                setPlayerState(PlayerState.PLAYER_2);
                this.activeFactory.reset();
                this.myShip = activeFactory.getNextShipsPlayer().shiftedToStartPosition(activeSetUpBoard);
                break;
            case PLAYER_2:
                // switch to player 1
                if (countNumberOfCellsWithShips() == false){
                    freezeBoard();
                } else unfreezeBoard();

                setActiveSetUpBoard(setUpBoard1);
                setInactiveSetUpBoard(setUpBoard2);
                setPlayerState(PlayerState.PLAYER_1);
                break;
        }    
        }
        return currentPlayer;
    }  
         
   
    @Override
    public PlayerState changePlayerInAttackMode() {
        if (getGameState() == GameState.ATTACK || getGameState() == GameState.WAITING){

            switch (currentPlayer) {
                case PLAYER_1:
                    setActiveAttckBoard(attackBoard2);
                    setInactiveAttckBoard(attackBoard1);
                    setPlayerState(PlayerState.PLAYER_2);
                    break;
                case PLAYER_2:
                    setActiveAttckBoard(attackBoard1);
                    setInactiveAttckBoard(attackBoard2);
                    setPlayerState(PlayerState.PLAYER_1);
                    break;
            }
        }
        return currentPlayer; 
    }

    @Override
    public void allShipsAreDown() {
        int count = 0;
        for (GridCell<Character> cell : activeAttackBoard) {
            if (cell.value() != '-' && cell.value() != 'm'){
                count++; 
            }
            if (count >= 26){
                setGameState(GameState.YOU_WON);
                freezeBoard(); 
            }
        } 
    }   


    @Override
    public boolean shotsFired(CellPosition pos) {
        char shownValue = activeAttackBoard.get(pos);
        if (shownValue != '-'){
            return false;
        }
        if (getPlayerState() == PlayerState.PLAYER_1){
            char value = setUpBoard2.get(pos);
            
            if (value != '-') { 
                activeAttackBoard.set(pos, 'h');
                System.out.println("Shot hit enemy`s ship");
            }
            else{ activeAttackBoard.set(pos, 'm');
            System.out.println("Shot missed");
            
            }
        }
        if (getPlayerState() == PlayerState.PLAYER_2){
            char value = setUpBoard1.get(pos);
            
            if (value != '-') { 
                activeAttackBoard.set(pos, 'h');
                System.out.println("Shot hit enemy`s ship");
            }
            else{ activeAttackBoard.set(pos, 'm');
            System.out.println("Shot missed");  
           
            }
        }
        return true;
    }

@Override
    public int shipsPlaced(){
        return this.shipsPlaced;
    }
}


   


   

    

   
    
   
    



    
    
    
    
    

