package no.uib.inf101.sem2.BattleShip.model;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;

public class Board extends Grid<Character>{
    /**
     * Constructs a new Board with the given number of rows and columns, initializing
     * each cell with the character '-'
     * @param rows the number of rows in the grid
     * @param cols the number of columns in the grid
     */
    public Board(int rows, int cols) {
        super(rows, cols);
    for (int r =0; r<rows; r++){
        for (int c=0; c<cols; c++){
            CellPosition cellPos = new CellPosition(r, c);
            set(cellPos, '-');
             }
        }    
    }
}
