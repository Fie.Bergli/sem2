package no.uib.inf101.sem2.BattleShip.view;
import java.awt.Color;

public interface ColorTheme {

    /** getCellColor() holds colors for the specific ships
     * @param character value which represents a ship
     * @returns a spesific color for the character 
     */
    Color getCellColor(Character character);
    
    /** getFrameColor() gets color for frame of the board 
     * @return new color 
    */
    Color getFrameColor();

    /**
     * getBackroundColor() gets color for backround of the board
     * @return new color 
     */
    Color getBackgroundColor();

    /**
     * gameStateColor() holds a spesific color, and is only activated when gamestate is game over
     * @return new color
     */
    Color gameStateColor();
}
    

