package no.uib.inf101.sem2.BattleShip.model.Ships;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public final class Ships implements Iterable<GridCell<Character>> {

    private char shipName;
    private boolean[][] shape;
    private CellPosition pos;

    public Ships(char shipName, boolean[][] shape, CellPosition pos) {
        this.shipName = shipName;
        this.pos = pos;
        this.shape = shape;
    }

    /**
     * creates a new Ship using boolean values.
     * 
     * A ship has three charachteristics:
     * 1 - a symbol that represents its "ship-group". All ships with this symbol
     * have the same color
     * 2 - a shape, for example a length of two or three cells, standing or vertical etc.
     * 3 - a position on board
     * 
     * @param shipName char representing a special shape
     * @return a new Ship with shipname boolean shape
     */
    public static Ships newShips(char shipName) {

        boolean[][] shape = switch (shipName) {
            //evt her endre fra boolean[][] til string[][]

            case '1' -> shape = new boolean[][] {
                    { false, false, false },
                    { true, true, false },
                    { false, false, false }
            };

            case '2' -> shape = new boolean[][] {
                    { false, false, false },
                    { true, true, true },
                    { false, false, false }
            };

            case '3' -> shape = new boolean[][] {
                    { false, false, false, false},
                    { true, true, true, true},
                    { false, false, false, false}, 
                    { false, false, false, false}
            };

            case '4' -> shape = new boolean[][] {
                    { false, false, false, false, false},
                    { false, true, false, false, false },
                    { true, true, true, true, true },
                    { false, true, false, false, false }, 
                    { false, false, false, false, false}
            };

            case '5' -> shape = new boolean[][] {
                    { false, false, false, false, false},
                    { false, false, false, false, false },
                    { true, true, true, true, true },
                    { true, true, true, true, true },
                    { false, false, false, false, false }
            };

            default -> throw new IllegalArgumentException("Uknown ship '" + shipName + "'");
        };
        return new Ships(shipName, shape, new CellPosition(0, 0));
    }

    @Override
    public Iterator<GridCell<Character>> iterator() {
        ArrayList<GridCell<Character>> shipList = new ArrayList<>();

        for (int row = 0; row < shape.length; row++) {
            for (int col = 0; col < shape[0].length; col++)

                if (shape[row][col]) {
                    shipList.add(new GridCell<Character>(new CellPosition(pos.row() + row, pos.col() + col), shipName));
                }
        }
        return shipList.iterator();
    }

    //autogenererte metoder
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + shipName;
        result = prime * result + Arrays.deepHashCode(shape);
        result = prime * result + ((pos == null) ? 0 : pos.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Ships other = (Ships) obj;
        if (shipName != other.shipName)
            return false;
        if (!Arrays.deepEquals(shape, other.shape))
            return false;
        if (pos == null) {
            if (other.pos != null)
                return false;
        } else if (!pos.equals(other.pos))
            return false;
        return true;
    }

    /**
     * Returns a new Ships object with its position shifted by deltaRow and deltaCol.
     * @param deltaRow the number of rows to shift the ship by
     * @param deltaCol the number of columns to shift the ship by
     * @return a new Ships object with its position shifted
     */
    public Ships shiftedBy(int deltaRow, int deltaCol){
        CellPosition posBy = new CellPosition(pos.row()+deltaRow, pos.col()+deltaCol);
        return new Ships(shipName, shape, posBy);
    }

    /**
     * Returns a new Ships object shifted to the start position for the given grid.
     * @param grid the grid for which to get the start position
     * @return a new Ships object shifted to the start position
     */
    public Ships shiftedToStartPosition(GridDimension grid){
        int posTopCenter = grid.cols()/2 - (shape.length/2);
        return shiftedBy(-1, posTopCenter);
    }

   /**
     * Returns a new Ships object with its shape rotated 90 degrees to the left.
     * @return a new Ships object with its shape rotated
     */
    public Ships rotateShip(){
        boolean[][] rotatingShip = new boolean[shape[0].length][shape.length];
        for (int row = 0; row < rotatingShip.length; row++){
            for (int col=0; col < rotatingShip[0].length; col++){
                rotatingShip[shape.length-1-col][row] = shape[row][col];
            }
        } return new Ships(shipName, rotatingShip, pos);
    }  
    
    /**
     * Returns the symbol for this ship.
     * @return the char symbol
     */
    public char getSymbol() {
        return this.shipName;
    }

    /**
     * returns the shape of the ship.
     * @return the two-dimensional array
     */
    public boolean[][] getShape() {
        return shape;
    }

}
