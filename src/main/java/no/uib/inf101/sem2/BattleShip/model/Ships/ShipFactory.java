package no.uib.inf101.sem2.BattleShip.model.Ships;

public interface ShipFactory {
    
    /** 
     * getNext() method gets next Ship from a string, which holds a 
     * number of letters representing a the shape  of a shape. 
     * @returns a new Ship. 
     */
    Ships getNextShipsPlayer();

    /**
     * gets index integer
     * @returns int
     */
    int getIndex();

    /**
     * resets index integer
     */
    void reset();
    
}
