package no.uib.inf101.sem2.model;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import no.uib.inf101.sem2.BattleShip.model.Ships.Ships;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

public class TestShips {

  

@Test
public void testHashCodeAndEquals() {
  Ships ship_1 = Ships.newShips('1');
  Ships ship_2 = Ships.newShips('1');
  Ships ship_3 = Ships.newShips('3').shiftedBy(1, 0);
  Ships ship_4 = Ships.newShips('4');
  Ships ship_5 = Ships.newShips('4').shiftedBy(0, 0);

  assertEquals(ship_1, ship_2);
  assertEquals(ship_4, ship_5);
  assertEquals(ship_1.hashCode(), ship_2.hashCode());
  assertEquals(ship_4.hashCode(), ship_5.hashCode());
  assertNotEquals(ship_1, ship_3);
  assertNotEquals(ship_1, ship_5);
}
    @Test
public void ShipIterationOf5() {
  // Create a standard '5' ship placed at (10, 100) to test
  Ships ship_5 = Ships.newShips('5');
  ship_5 = ship_5.shiftedBy(10, 100);

  // Collect which objects are iterated through
  ArrayList<GridCell<Character>> objs = new ArrayList<>();
  for (GridCell<Character> gc : ship_5) {
    objs.add(gc);
  }

  // Check that we got the expected GridCell objects
  assertEquals(10, objs.size());
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 100), '5')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 101), '5')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 102), '5')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 103), '5')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 104), '5')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(13, 100), '5')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(13, 101), '5')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(13, 102), '5')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(13, 103), '5')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(13, 104), '5')));
    }

    @Test
public void ShipIterationOf4() {
  // Create a standard '4' ship placed at (10, 100) to test
  Ships ship_4 = Ships.newShips('4');
  ship_4 = ship_4.shiftedBy(10, 100);

  // Collect which objects are iterated through
  ArrayList<GridCell<Character>> objs = new ArrayList<>();
  for (GridCell<Character> gc : ship_4) {
    objs.add(gc);
  }

  // Check that we got the expected GridCell objects
  assertEquals(7, objs.size());
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 100), '4')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(11, 101), '4')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 101), '4')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(13, 101), '4')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 102), '4')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 103), '4')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 104), '4')));
}
    
  
}

