package no.uib.inf101.sem2.model;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.BattleShip.model.BattleShipModel;
import no.uib.inf101.sem2.BattleShip.model.Board;
import no.uib.inf101.sem2.BattleShip.model.PlayerState;
import no.uib.inf101.sem2.BattleShip.model.Ships.ShipFactory;
import no.uib.inf101.sem2.BattleShip.model.Ships.Ships;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

public class TestBattleShipModel {

    @Test
    public void testMoveShip(){
      Ships ship_3 = Ships.newShips('3');
      ship_3 = ship_3.shiftedBy(10, 10);
      ship_3 = ship_3.shiftedBy(10, 10); 
    
      ArrayList<GridCell<Character>> objs = new ArrayList<>();
      for (GridCell<Character> gc : ship_3) {
        objs.add(gc);
      }
    
      assertEquals(4, objs.size());
      assertTrue(objs.contains(new GridCell<>(new CellPosition(21, 20), '3')));
      assertTrue(objs.contains(new GridCell<>(new CellPosition(21, 21), '3')));
      assertTrue(objs.contains(new GridCell<>(new CellPosition(21, 22), '3')));
      assertTrue(objs.contains(new GridCell<>(new CellPosition(21, 23), '3')));
    }
    
    
        @Test
    public void testShiftedToStartPosition3(){
      
      Ships testShip = Ships.newShips('3');
      testShip = testShip.shiftedToStartPosition(new Board(8, 8) );
    
      ArrayList<GridCell<Character>> objsPos = new ArrayList<>();
      for (GridCell<Character> gcEven : testShip) {
        objsPos.add(gcEven);
      }
    
      //even
     
      assertTrue(objsPos.contains(new GridCell<>(new CellPosition(0, 3), '3')));
      assertTrue(objsPos.contains(new GridCell<>(new CellPosition(0, 4), '3')));
      assertTrue(objsPos.contains(new GridCell<>(new CellPosition(0, 2), '3')));
      assertTrue(objsPos.contains(new GridCell<>(new CellPosition(0, 5), '3')));
    }
    
    
    @Test
    public void testshiftedToStartPosition2(){
        
        Ships shiptest = Ships.newShips('2');
        shiptest = shiptest.shiftedToStartPosition(new Board(5, 5) );
      
        ArrayList<GridCell<Character>> objsPos = new ArrayList<>();
        for (GridCell<Character> gcOdd : shiptest) {
          objsPos.add(gcOdd);
        }
    
      assertTrue(objsPos.contains(new GridCell<>(new CellPosition(0, 2), '2')));
      assertTrue(objsPos.contains(new GridCell<>(new CellPosition(0, 3), '2')));
      assertTrue(objsPos.contains(new GridCell<>(new CellPosition(0, 1), '2')));
      
      }
    
    @Test
    public void testRotateShip() {
      // Create a standard '2' ship placed at (10, 100) to test
      Ships ship = Ships.newShips('2');
      ship = ship.shiftedBy(10, 100);
      ship = ship.rotateShip();
    
      // Collect which objects are iterated through
      ArrayList<GridCell<Character>> objs = new ArrayList<>();
      for (GridCell<Character> gc : ship) {
        objs.add(gc);
      }
     
     assertTrue(objs.contains(new GridCell<>(new CellPosition(10, 101), '2')));
     assertTrue(objs.contains(new GridCell<>(new CellPosition(11, 101), '2')));
     assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 101), '2')));
    
    }
  
    @Test
    public void testShotsFired() {

      Board board1 = new Board(10,10);
      Board board2 = new Board(10,10);
      Board board3 = new Board(10,10);
      Board board4 = new Board(10,10);
      Ships ship = Ships.newShips('3');
      ShipFactory factory = new ShipFactory();
      ship = factory.getNextShipsPlayer().shiftedToStartPosition(activeSetUpBoard);

      BattleShipModel model = new BattleShipModel(board1, board2, board3, board4, factory);

    // Set up Player 1's board
    model.setPlayerState(PlayerState.PLAYER_1);
    model.moveShip(0, 0);
    model.gluedShip();
    model.moveShip(1, 0);
    model.gluedShip();
    model.moveShip(2, 0);
    model.gluedShip();

    // Set up Player 2's board
    model.setActiveSetUpBoard(board1);
    model.setPlayerState(PlayerState.PLAYER_2);
    model.moveShip(0, 0);
    model.gluedShip();
    model.moveShip(1, 0);
    model.gluedShip();
    model.moveShip(2, 0);
    model.gluedShip();

    // Fire shots
    model.setActiveAttackBoard(model.attackBoard1);
    model.setPlayerState(PlayerState.PLAYER_1);
    assertTrue(model.shotsFired(0, 0)); // hit
    assertFalse(model.shotsFired(0, 1)); // miss
    assertTrue(model.shotsFired(1, 0)); // hit
    assertFalse(model.shotsFired(1, 1)); // miss
    assertTrue(model.shotsFired(2, 0)); // hit
    assertFalse(model.shotsFired(2, 1)); // miss

    // Switch to Player 2
    model.changePlayer();

    // Fire more shots
    model.setActiveAttackBoard(model.attackBoard2);
    assertTrue(model.shotsFired(0, 0)); // hit
    assertFalse(model.shotsFired(0, 1)); // miss
    assertTrue(model.shotsFired(1, 0)); // hit
    assertFalse(model.shotsFired(1, 1)); // miss
    assertTrue(model.shotsFired(2, 0)); // hit
    assertFalse(model.shotsFired(2, 1)); // miss

    // Test end game condition








    }



        
    
      
}
