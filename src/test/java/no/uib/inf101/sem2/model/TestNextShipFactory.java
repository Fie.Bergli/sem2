package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import no.uib.inf101.sem2.BattleShip.model.Ships.NextShipFactory;
import no.uib.inf101.sem2.BattleShip.model.Ships.ShipFactory;
import no.uib.inf101.sem2.BattleShip.model.Ships.Ships;

public class TestNextShipFactory {

  @Test
  public void testGetNextShipsPlayerLoop() {
      ShipFactory factory = new NextShipFactory();
  
      // Test that the first ship is '1'
      Ships ship = factory.getNextShipsPlayer();
      assertEquals('1', ship.getSymbol());
  
      // Test that the second ship is '2'
      ship = factory.getNextShipsPlayer();
      assertEquals('2', ship.getSymbol());
  
      // Test that the third ship is '3'
      ship = factory.getNextShipsPlayer();
      assertEquals('3', ship.getSymbol());
  
      // Test that the fourth ship is '4'
      ship = factory.getNextShipsPlayer();
      assertEquals('4', ship.getSymbol());
  
      // Test that the fifth ship is '5'
      ship = factory.getNextShipsPlayer();
      assertEquals('5', ship.getSymbol());
  
      // Test that the sixth ship is '1' again (looping)
      ship = factory.getNextShipsPlayer();
      assertEquals('1', ship.getSymbol());
  }
  

    
}
