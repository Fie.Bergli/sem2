# Mitt program

Hei bruker, velkommen til min utgave av BattleShip. 
Dette er et spill ment for to spillere, så her må du hente en venn for å noen å spille mot. 

For å spille battleship må du følge instruksene som kommer opp på skjermen. Du blir møtt av en velkomstskjerm som introduserer deg for spillet, og gir deg beskjed om å trykke på '1' tasten for å starte å spillet. Trykk derfor på '1' tasten din, så er spillet i gang! 

Nå er du i en ny fase av spillet, SET-UP fasen. Her setter du opp ditt eget brett, ved å plassere skipene dine et valgfritt sted på brettet. Her er det kun to regler: skipene kan ikke plasseres på et annet skip, og skipet må plasseres innenfor brettets rammer. Du flytter på skipene dine ved hjelp av piltastene på tastaturet ditt, roterer skipet ved hjelp av 'R' tasten og til slutt plasserer skipet ved hjelp av space tasten. Plasser alle fem skip som kommer på brettet, og snu pc-skjermen til motspilleren. Nå er det motspilleren sin tur til å plassere skip, på samme måte.

Når alle skip er plassert på brettet kan dere begynne å spille mot hverandre. Nå vil første spiller begynne med å få mulighet til å gjette en posisjon på motspillerens brett, hvor han eller hun tror motspilleren har plassert et skip. Her bruker du museklikk for å gjette en posisjon på brettet. Ruten du har gjettet på, vil enten bli rød om du bommet eller grønn om du traff motstanders skip. Deretter er det motspillerens tur. Dette fortsetter, og nå er det førstemann til å gjette motspillerens alle 5 skip. Den som klarer det først, vinner!

video link:
https://youtu.be/IZ2yORwMpyY